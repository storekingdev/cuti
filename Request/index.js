var http = require("http");
var puttu = require("puttu-redis");
var _ = require("lodash");
var masterName = null;
function init(_masterName) {
    masterName = _masterName;
}
function getOptions(url, method, path, magicKey) {
    var options = {};
    path = url.split("/");
    options.hostname = url.split("//")[1].split(":")[0];
    options.port = url.split(":")[2].split("/")[0];
    options.path = "/" + path.splice(3, path.length - 3).join("/");
    options.method = method;
    options.headers = {};
    options.headers["content-type"] = "application/json";
    options.headers["magicKey"] = magicKey ? magicKey : null;
    return options;
}
function getUrlandMagicKey(masterName, retries) {
    if (!retries) /* then */ retries = Date.now();
    return puttu.get(masterName)
        .then(url => {
            return puttu.getMagicKey(masterName)
                .then(magicKey => {
                    var options = {};
                    var path = url.split("/");
                    options.hostname = url.split("//")[1].split(":")[0];
                    options.port = url.split(":")[2].split("/")[0];
                    options.path = "/" + path.splice(3, path.length - 3).join("/");
                    options.method = "GET";
                    options.headers = {};
                    options.headers["masterName"] = masterName;
                    options.headers["content-type"] = "application/json";
                    options.headers["magicKey"] = magicKey ? magicKey : null;
                    return new Promise(resolve => resolve(options));
                }, err => console.error("error from prehooks internal", err));
        }, err => Date.now() - retries < 500 ? getUrlandMagicKey(masterName, retries) : new Promise((resolve, reject) => reject(new Error(masterName + " Service down"))));
}
function checkIfExists(masterName, id) {
    return new Promise((resolve, reject) => {
        getUrlandMagicKey(masterName)
            .then(options => {
                options.path += "/" + id;
                http.request(options, response => response.statusCode === 200 ? resolve() : reject(new Error("Invalid " + masterName))).end();
            }, err => reject(err));
    });
}
function getElement(masterName, id, select) {
    select = select ? "?select=" + select : "";
    return getUrlandMagicKey(masterName)
        .then(options => {
            options.path += "/" + id + select;
            return new Promise((resolve, reject) => {
                http.request(options, response => {
                    if (response.statusCode != 200) {
                        reject(new Error(masterName + " return with statusCode " + response.statusCode));
                    }
                    else {
                        var data = "";
                        response.on("data", _data => data += _data.toString());
                        response.on("end", () => resolve(JSON.parse(data)));
                    }
                }).end();
            });
        });
}
function getServiceEntitlements(service, franchise) {
    return new Promise((resolve) => {
        getUrlandMagicKey("franchise")
            .then(options => {
                options.path += "/" + franchise;
                options.method = "GET";
                http.request(options, response => {
                    var data = "";
                    response.on("data", _data => data += _data.toString());
                    response.on("end", () => {
                        var franchise = JSON.parse(data);
                        resolve(franchise.services[service]);
                    });
                }).end();
            });
    });
}


/**
 * @author Aman Kareem <aman.kareem@storeking.in>
 * @description This function wraps the function dependent parameteres into http req and res format; 
 *              USAGE: Use this function to invoke another function which takes req and res as params, with in the same module , 
 *              Instead of actually making HTTP call, you use this to invoke any function which accepts req and res; 
 *              Pass the parameters as in you pass it wrt to swagger definition to any api endpoint within the same service;
 *              This function wraps your params into HTTP req and res mocker;
 *              
 * @param {*} headers - Custom headers if any
 * @param {*} payload - As per swagger definition
 * @param {*} httpBody - Just http body
 * @param {*} interceptor - Call back to handle response or error
 */
function httpInternalizer(headers, payload, httpBody, interceptor) {

    var req = {
        "swagger": { "params": {} },
        "headers": headers ? headers : {}
    };

    if (httpBody && httpBody.createdBy) {
        req.user = { username: httpBody.createdBy, _id: httpBody.createdBy };
    }

    if (headers && headers.username) {
        req.user = { username: headers.username };
    }

    if (headers && headers._id) {
        req.user = { _id: headers._id };
    }

    Object.keys(payload).map(k => {
        req.swagger.params[k] = { value: payload[k] };
    });

    if (httpBody) {
        req.body = httpBody;
    }

    var res = {
        "status": function (statusCode) {
            return {
                "statusCode": statusCode,
                "send": function (data) {
                    if (statusCode === 200) {

                        if (data.constructor.name === 'model') {
                            data = data.toObject();
                        }

                        interceptor(null, data);
                    }
                    else {
                        interceptor(data); //Error
                    }
                },
                "json": function (data) {
                    if (statusCode === 200) {

                        if (data.constructor.name === 'model') {
                            data = data.toObject();
                        }

                        interceptor(null, data);
                    }
                    else {
                        interceptor(data); //Error
                    }
                }
            }
        }
    };

    return { req: req, res: res }
}

/**
 * @author Aman Kareem <aman.kareem@storeking.in>
 * @description Common Http request making function block;
 * @param {*String} _magickey //Redis-Key;
 * @param {*String} _path //API Path;
 * @param {*String} _method // Http method - POST , PUT , GET;
 * @param {*Object} _payload //Requset body;
 */
function fireHttpRequest(_magickey, _path = null, _method, _payload, _headers) {
    return new Promise((resolve, reject) => {
        
        if (!_magickey) {
            reject(new Error("Magic Key cannot be empty for HTTP request."));
            return;
        }
        
        if (!_method) {
            reject("Http Method cannot be empty for HTTP request.");
            return;
        }

        getUrlandMagicKey(_magickey)
            .then(options => {

                if (_path && _path !== " " || !_.isEmpty(_path.trim())) options.path += _path;

                options.method = _method;

                if (_headers) {
                    options.headers = Object.assign(options.headers, _headers);
                }

                var request = http.request(options, response => {
                    var data = "";
                    response.on("data", _data => data += _data.toString());
                    response.on("end", () => {
                        if (response.statusCode === 200) {
                            try {
                                data = JSON.parse(data);
                                resolve(data);
                            } catch (e) {
                                reject(e);
                            }
                        } else {
                            reject(new Error(data));
                        }
                    });
                });
                if ((_method === "POST" || _method === "PUT" ||  _method === "PATCH") && !_.isEmpty(_payload))
                    request.end(JSON.stringify(_payload));
                else
                    request.end();

                request.on('error', function (err) {
                    console.error(`[ SOCKET HANG-UP ] Response not handled or resolved by API - ${_magickey} - ${options.path} \n`, err);
                    reject(err);
                });

            }).catch(e => reject(e));
    });
}

module.exports.getServiceEntitlements = getServiceEntitlements;
module.exports.getElement = getElement;
module.exports.getOptions = getOptions;
module.exports.getUrlandMagicKey = getUrlandMagicKey;
module.exports.checkIfExists = checkIfExists;
module.exports.fireHttpRequest = fireHttpRequest;
module.exports.httpInternalizer = httpInternalizer;
module.exports.init = init;