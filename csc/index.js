var Mongoose = require("mongoose");
var SMCrud = require("swagger-mongoose-crud");

var cscLogsdefinition = require("./csclog.model").cscLogsDefinition;
var cscLogsSchema = new Mongoose.Schema(cscLogsdefinition);
var cscLogsCrudder = new SMCrud(cscLogsSchema, "cscLogs");


function createLog(data, type, headers) {
    return new Promise((resolve, reject) => {
        let logData = {"apiType": type, payload: data, headers: headers };
        cscLogsCrudder.model.create(logData, (err, doc) => {
            if(err)  {
                reject({message: "Unable to log request"});
            } else {
                resolve(doc);
            }
        });
    });
}

function updateLog(id, updateData, pushData) {
    return new Promise((resolve, reject) => {
        let findQuery = {"_id": id };
        let updateQuery = {"$set": updateData };

        if(pushData) {
            updateQuery["$push"] = pushData;
        }

        cscLogsCrudder.model.findOneAndUpdate(findQuery, updateQuery, (err, doc) => {
            resolve();
        });
    });
}

function findOne(findQuery, select) {
    return new Promise((resolve, reject) => {
        cscLogsCrudder.model.findOne(findQuery, select, (err, doc) => {
            resolve(doc);
        });
    });
}

function find(findQuery, select) {
    return new Promise((resolve, reject) => {
        cscLogsCrudder.model.find(findQuery, select, (err, docs) => {
            resolve(docs);
        });
    });
}


module.exports = {
    createLog,
    updateLog,
    findOne,
    find
};