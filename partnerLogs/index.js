var Mongoose = require("mongoose");
var SMCrud = require("swagger-mongoose-crud");

var logsdefinition = require("./partnerLogs.model").logsDefinition;
var logsSchema = new Mongoose.Schema(logsdefinition);
var logsCrudder = new SMCrud(logsSchema, "partnerIntegrationLogs");


function createLog(data, type, headers, partner, orderId) {
    return new Promise((resolve, reject) => {
        let logData = {partner: partner, "apiType": type, payload: data, headers: headers, orderId: orderId };
        logsCrudder.model.create(logData, (err, doc) => {
            if(err)  {
                reject({message: "Unable to log request"});
            } else {
                resolve(doc);
            }
        });
    });
}

function updateLog(id, updateData, pushData) {
    return new Promise((resolve, reject) => {
        let findQuery = {"_id": id };
        let updateQuery = {"$set": updateData };

        if(pushData) {
            updateQuery["$push"] = pushData;
        }

        logsCrudder.model.findOneAndUpdate(findQuery, updateQuery, (err, doc) => {
            resolve();
        });
    });
}

function findOne(findQuery, select) {
    return new Promise((resolve, reject) => {
        logsCrudder.model.findOne(findQuery, select, (err, doc) => {
            resolve(doc);
        });
    });
}

function find(findQuery, select) {
    return new Promise((resolve, reject) => {
        logsCrudder.model.find(findQuery, select, (err, docs) => {
            resolve(docs);
        });
    });
}


module.exports = {
    createLog,
    updateLog,
    findOne,
    find
};