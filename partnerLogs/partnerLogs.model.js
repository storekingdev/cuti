var Mongoose = require("mongoose");
let logsDefinition = {
    partner: { type: String, enum: ["CSC", "GPAY"]},
	apiType: { type: String },
	orderId: { type: String },
    payload: {type: Mongoose.Schema.Types.Mixed},
    actualToken: { type: String },
    expiresOn: {type: Date},
    response: {type: Mongoose.Schema.Types.Mixed},
    headers: {type: Mongoose.Schema.Types.Mixed},
    createdAt: {
        type: Date
    },
    lastUpdated: {
        type: Date
    }
};

module.exports = {
    logsDefinition
};